public class opAritmeticos{
  public static void main(String[] args) {
    int j, k, m;
    int d= 123;
    j= d--; //j vale 122 y d vale 123
    System.out.println("j= " + j);
    k= ++d; //k vale 124 y d vale 124
    System.out.println("k= "+ k);
    m= --d; //m vale 123 y d vale 123
    System.out.println("m= "+ m);
    m=k % j; //operador Resto para los tipos int

    //k=124 y j=122 por tanto,m= 2
    System.out.println("m= " + m);
    j=5; k=3; m=j/k; //division entera m=1
    System.out.println("m= " + m);
    System.exit(0);
  }
}
